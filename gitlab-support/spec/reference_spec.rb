# frozen_string_literal: true

require_relative '../reference'

RSpec.describe Reference do
  describe '#construct' do
    it 'shortens a GitHub issue' do
      url = 'https://github.com/federico-terzi/espanso/issues/487#issuecomment-858903403'
      expect(Reference.new(url).construct)
        .to eq('federico-terzi/espanso#487')
    end

    it 'abbreviates common names' do
      url = 'https://gitlab.com/gitlab-com/support/support-team-meta/-/issues/3539#note_597649648'
      expect(Reference.new(url).construct)
        .to eq('STM#3539')

      # We should test the reg-ex dependent case as well, but not all other abbreviatons that work like the STM case
      url = 'https://gitlab.com/gitlab-com/dev-sub-department/section-dev-request-for-help/-/issues/123'
      expect(Reference.new(url).construct)
        .to eq('dev-RfH#123')
    end

    it 'shortens a GitLab issue with design' do
      url = 'https://gitlab.com/gitlab-org/gitlab/-/issues/332281/designs/slack-notifications-current.png'
      expect(Reference.new(url).construct)
        .to eq('gitlab#332281')
    end

    it 'shortens a GitLab MR with diff' do
      url = 'https://gitlab.com/gitlab-com/support/toolbox/espanso/-/merge_requests/1/diff'
      expect(Reference.new(url).construct)
        .to eq('toolbox/espanso!1')
    end

    it 'shortens a GitLab epic' do
      url = 'https://gitlab.com/groups/gitlab-com/support/toolbox/-/epics/1'
      expect(Reference.new(url).construct)
        .to eq('toolbox&1')
    end

    it 'do not shorten a top-level item too much' do
      url = 'https://gitlab.com/groups/gitlab-com/-/epics/1'
      expect(Reference.new(url).construct)
        .to eq('gitlab-com&1')
    end

    it 'raises an error for empty input' do
      expect(Reference.new(ENV['SOME_CLIPBOARD']).construct)
        .to eq ('')
    end
  end
end
