#!/bin/env ruby
# frozen_string_literal: true

# Progressively remove non-ID (and superfluous) URL parts
# https://github.com/federico-terzi/espanso/issues/487#issuecomment-858903403
class Reference
  attr_reader :url

  def initialize(url)
    @url = url || ''
  end

  def construct
    return '' if url.nil? || url.empty?

    tmp = url
      .sub(%r{^https?://git(la|hu)b.com}, '') # Remove domain
      .sub('/groups', '')                     # Remove entity prefix (for non-project epics)
    tmp = trim_top_only_from_subgroups(tmp)

    ref = tmp
      .sub(%r{[/\\]?#[\w-]+$}, '')            # Remove anchor text
      .sub(%r{[/\\](diff|design).*$}, '')     # Remove sub-entities
      .sub('/support', '')                    #   and sub-level group names
      .sub('/-', '')                          # Remove routing separator (gitlab#214217)
      .sub(%r{/+}, '/')                       # Remove additional,
      .delete_prefix('/')                     #   superfluous slashes

    ref = type_to_symbol(ref)
    return abbreviate_common(ref)
  end

  private

  def trim_top_only_from_subgroups(string)
    return string unless subgroup_present?(string)
    string.sub(%r{^/gitlab-(com|org)}, '')
  end

  def subgroup_present?(string)
    string.split('/').length > 5
    # Magic 5 here, because that's the array length of top-only paths
    # after the first url.sub operations were done.
  end

  def type_to_symbol(string)
    string
      .sub('/epics/',          '&')
      .sub('/issues/',         '#')
      .sub('/merge_requests/', '!')
      .sub('/milestones/',     '%')
  end

  def abbreviate_common(string)
    string
      .sub('content-sites/handbook', 'HB')
      .sub('content-sites/internal-handbook', 'iHB')
      .sub('request-for-help', 'RfH')
      .sub(%r{\w+-sub-department/section-}, '')
      .sub('support-team-meta', 'STM')
      .sub('ticket-attention-requests', 'STAR')
      .sub('www-gitlab-com', 'WWW')
  end
end

puts Reference.new(ARGV.first).construct
