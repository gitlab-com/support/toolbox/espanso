# GitLab Support snippets for Espanso.org

An experiment in GitLab-shared text expansions/snippets.
See also [our discussion in support-team-meta#3539](https://gitlab.com/gitlab-com/support/support-team-meta/-/issues/3539#note_597649648).

## Prerequisites

The gems from [our `Gemfile`](Gemfile) need to be installed to use our (GitLab) expansions:

```shell
bundle install
```

## Installation

This is [an "external" package](https://espanso.org/docs/packages/external-packages/).

```shell
espanso install gitlab-support --external --git https://gitlab.com/gitlab-com/support/toolbox/espanso \
  --force  # to overwrite/upgrade already installed package
  --git-branch=…MR… # for testing/reviewing MRs
```

## Usage

Please review the list of triggers and replacement in [the `package.yml` file](gitlab-support/package.yml).

## Contributing

MRs are welcome! For major changes, please open an issue first to discuss what you would like to change.

[Install and activate lefthook](https://github.com/evilmartians/lefthook/blob/master/docs/full_guide.md#installation)
to benefit from the `pre-commit` automations.

MR reviewers, please use [the `--git-branch=` flag](#installation) to install the MR contents,
or [run `make inject`](Makefile) after checking out the MR branch locally.

### Development dependencies

Appending the up-to-date table of triggers and espansions requires:
Make, perl, awk,
[jq](https://stedolan.github.io/jq/), and
[yq](https://mikefarah.gitbook.io/yq/).

If you don't want to install all those, you can
[skip the (left)hooks](https://github.com/evilmartians/lefthook/blob/master/docs/full_guide.md#skip-lefthook-execution).

## Other useful/noteworthy Espanso packages

- @katrinleinweber's [effective-markdown](https://github.com/katrinleinweber/espanso-effective-markdown) & [greetings-english](https://github.com/katrinleinweber/espanso-greetings-english)
- [Mac Symbols](https://hub.espanso.org/packages/mac-symbols/)
- [Shruggie](https://hub.espanso.org/packages/shruggie/)
- [What the Commit](https://hub.espanso.org/packages/wtc/)

## List of included espansions

Trigger | Espansion
------- | ---------
``` [gfs ``` | ``` [run `fast-stats $\|$`](https://gitlab.com/gitlab-com/support/toolbox/fast-stats/#when-to-use-it) (binaries available [here](https://gitlab.com/gitlab-com/support/toolbox/fast-stats/-/releases)) ```
``` [jq ``` | ``` [jq](https://docs.gitlab.com/ee/administration/troubleshooting/log_parsing.html) ```
``` [Rc ``` | ``` [the Rails console](https://docs.gitlab.com/ee/administration/operations/rails_console.html) ```
``` [rg ``` | ``` [ripgrep](https://github.com/BurntSushi/ripgrep/blob/master/GUIDE.md) ```
``` [sd ``` | ``` [sd](https://github.com/chmln/sd#sd---search--displace) ```
``` [ug ``` | ``` [ugrep](https://github.com/Genivia/ugrep#readme) ```
``` [cih ``` | ``` When you have generated another set of logs, please feel free to try these helper tools:\n\n1. [download_ci_logs.rb](https://gitlab.com/gitlab-com/support/toolbox/dotfiles/-/blob/main/scripts/download_ci_logs.rb) get get multiple logs\n2. [list-slowest-job-sections](https://gitlab.com/gitlab-com/support/toolbox/list-slowest-job-sections/) to check for significantly different durations.\n3. [clean_job_logs_for_diff_ing.sh](https://gitlab.com/gitlab-com/support/toolbox/dotfiles/-/blob/main/scripts/clean_job_logs_for_diff_ing.sh) to help find relevant messages that make the logs diverge.\n ```
``` /sartpom ``` | ``` shortly after reproducing the problem once more. ```
``` /itarf ``` | ``` in the affected repository folder (which needs to be a full, up-to-date clone) ```
``` [gsos ``` | ``` [run our `GitLabSOS` script](https://gitlab.com/gitlab-com/support/toolbox/gitlabsos#run-the-script) {{sartpom}} ```
``` [ksos ``` | ``` [run our `KubeSOS` script](https://gitlab.com/gitlab-com/support/toolbox/kubesos#kubesos) {{sartpom}} ```
``` [gsz ``` | ``` [run `git-sizer`](https://github.com/github/git-sizer#getting-started) {{itarf}} ```
``` [har ``` | ``` [record a `.har` file](https://support.zendesk.com/hc/en-us/articles/4408828867098) while browsing to/through the problem once more. ```
``` [gfr ``` | ``` [run `git filter-repo --analyze`](https://github.com/newren/git-filter-repo/#how-do-i-install-it) {{itarf}} ```
``` \|ht ``` | ``` \| sort \| awk 'NR==1; END{print}'\n ```
``` \|fstp ``` | ``` \| fast-stats top --display=perc ```
``` \|jqtc ``` | ``` \| jq .time \| cut -d':' -f1-2 \| sort \| uniq -c ```
``` \|jqtsv ``` | ``` \| jq --raw-output '[.{{clipboard}}, .$\|$] \| @tsv' \| sort \| uniq -c \| sort ```
``` \|rgoj ``` | ``` \| rg --only-matching '\\{.*\\}' ```
``` /stc ``` | ``` /label ~"Support Team Contributions"\n/assign me\n/assign_reviewer \n/milestone %15. ```
``` /ber ``` | ``` bundle exec rspec {{clipboard}}\n ```
``` /glt ``` | ``` {{title}} ```
``` /glref ``` | ``` {{refer}} ```
``` [tr) ``` | ``` [{{title}} ({{refer}})]({{clipboard}}) ```
``` [rt) ``` | ``` [{{refer}} ({{title}})]({{clipboard}}) ```
``` [r) ``` | ``` [{{refer}}]({{clipboard}}) ```
``` ~cfb ``` | ``` The following ~customer is interested in this capability \n\n- Subscription: ~"GitLab Ultimate" OR ~"GitLab Premium" OR ~"GitLab Free" \n- Product: ~"self-managed" OR ~"gitlab.com" OR ~"SaaS Dedicated" \n- Link to request: {{clipboard}}\n- Priority: ~customer priority::\n- Why interested:\n- Problem they are trying to solve:\n- Current solution for this problem:\n- Impact to the customer of not having this:\n- Questions:\n- PM to mention: @\n- CSM to mention: @ ```
``` [sst ``` | ``` See [Slack thread]({{clipboard}})\n\n> $\|$ ```
``` .gcy ``` | ``` .gitlab-ci.yml ```
``` /eg ``` | ``` /etc/gitlab/ ```
``` .grb ``` | ``` gitlab.rb ```
``` .gsj ``` | ``` gitlab-secrets.json ```
``` `gcy ``` | ``` `{{gcy}}` ```
``` `grb ``` | ``` `{{grb}}` ```
``` `gsj ``` | ``` `{{gsj}}` ```
``` /vlg ``` | ``` /var/log/gitlab/ ```
``` /og ``` | ``` /opt/gitlab/ ```
``` /ebg ``` | ``` sudo -u git {{og}}embedded/bin/git ```
``` /vog ``` | ``` /var{{og}} ```
``` /vob ``` | ``` {{vog}}backups ```
``` /vodr ``` | ``` {{vog}}git-data/repositories/{{clipboard}} ```
``` /gctl ``` | ``` sudo gitlab-ctl tail \| tee /tmp/gl-{{clipboard}}-$\|$.txt ```
``` /glcf ``` | ``` sudo gitlab-ctl reconfigure ```
``` /glst ``` | ``` sudo gitlab-ctl status ```
``` /glrt ``` | ``` sudo gitlab-ctl restart ```
``` /glsp ``` | ``` sudo gitlab-ctl stop ```
``` /glsq ``` | ``` sudo gitlab-psql -c "SELECT  FROM  WHERE ;" ```
``` /glfc ``` | ``` grep --invert-match --ignore-case --extended-regexp "\\.{3} (finished\|skipped\|success\|yes)" *check* ```
``` &sde ``` | ``` shutdown && exit ```
``` /glsri ``` | ``` # Support-Resource initialization with a minimal configuration,\n# pinned version & some useful configs\nGRB=/etc/gitlab/gitlab.rb\n\n# Block Gravatar\nsudo echo '127.0.0.1 gravatar.com' >> /etc/hosts\n\n# Clear config file & insert a few useful items\nsudo sed \\\n    -e 's/#.*$//;/^$/d' \\\n    --in-place=.ori \\\n    $GRB && \\\necho "gitlab_rails['usage_ping_enabled'] = false" >> $GRB && \\\necho "logging['logrotate_frequency'] = nil" >> $GRB && \\\necho "logging['logrotate_size'] = '1G'" >> $GRB && \\\nsudo gitlab-ctl reconfigure &&\n\n# Install tools and upgrades without changing GitLab\nsudo apt-mark hold {*g,g}itlab* &&\napt install --yes ripgrep jq &&\nsudo apt --yes upgrade &&\nsudo reboot\n ```
``` *(Q ``` | ``` **(Q$\|$)**  ```
``` (ut ``` | ``` (using $\|$ there) ```
``` <ds ``` | ``` <details><summary>$\|$</summary>{{clipboard}}</details> ```
``` c-a-b: ``` | ``` Co-authored-by: $\|$ <@gitlab.com> ```
``` [ZD ``` | ``` [ZD {{clipboard}}](https://gitlab.zendesk.com/agent/tickets/{{clipboard}}) ```
``` [:z ``` | ``` :zendesk:[{{clipboard}}](https://gitlab.zendesk.com/agent/tickets/{{clipboard}}) ```
``` :glspair ``` | ``` :pair: :support-tanuki: $\|${{refer}} ```
``` :tfcc ``` | ``` :thread: for customer call about :zendesk: . cc @ ```
``` :tfe ``` | ``` :thread: for emergency :point_up: ```
``` /coufind ``` | ``` /chatops run user find {{clipboard}} ```
``` /confind ``` | ``` /chatops run namespace find {{clipboard}} ```
``` /conmin ``` | ``` /chatops run namespace minutes {{clipboard}} … ```
``` /cofsp ``` | ``` /chatops run feature set --project={{clipboard}} …FF… true/false ```
``` /cofsn ``` | ``` /chatops run feature set --namespace={{clipboard}} …FF… true/false ```
